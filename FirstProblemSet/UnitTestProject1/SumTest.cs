﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstPS;

namespace FirstPSTest
{
	[TestClass]
	public class SumTest
	{
		[TestMethod]
		public void SummirizeOfVectorsWithDifferentLength()
		{
			var vector1 = new VectorCode(new int[] { 1, 2, 3 });
			var vector2 = new VectorCode(new int[] { 1, 2, 3, 4 });
			Assert.ThrowsException<ArgumentException>(() => vector1.Sum(vector2));
		}
		[TestMethod]
		public void UsualSum()
		{
			var v1 = new VectorCode(new int[] { 1, 2, 0, 3, 4 });
			var v2 = new VectorCode(new int[] { 4, 0, 8, 9, 10 });
			var expArr = new int[] { 2, 4, 6, 8 };
			CompareSummedArrays(v1.Decode(), v2.Decode(), new int[] { 5, 2, 8, 12, 14 });
		}
		[TestMethod]
		public void SumWithInsertAndDelete()
		{
			var v1 = new VectorCode(new int[] { 1, 2, 0, 3, 4 });
			var v2 = new VectorCode(new int[] { 4, 0, 8, 9, 10 });
			v2.Insert(10, 1);
			v1.Delete(3);
			var expArr = new int[] { 2, 4, 6, 8 };
			CompareSummedArrays(v1.Decode(), v2.Decode(), new int[] { 5, 12, 8, 9, 14 });
		}
		[TestMethod]
		public void SumVectorsWithDifferentCoodrinates()
		{
			var arr1 = new int[] { 0, 2, 0, 0, 7, 8, 10, 1, 0 };
			var arr2 = new int[] { 1, 0, 3, 4, 8, 11, 0, 0, 1 };
			var expArr = new int[] { 1, 2, 3, 4, 15, 19, 10, 1, 1 };
			CompareSummedArrays(arr1, arr2, expArr);
		}
		[TestMethod]
		public void SumEmptyVectors()
		{
			var arr1 = new int[0];
			var arr2 = new int[0];
			var expArr = new int[0];
			CompareSummedArrays(arr1, arr2, expArr);
		}
		private void CompareSummedArrays(int[] array, int[] array2, int[] expArr)
		{
			var vector = new VectorCode(array);
			var vector2 = new VectorCode(array2);
			Assert.IsTrue(Comparing.AreArraysSame(expArr, vector.Sum(vector2).Decode()));
		}
	}
}
