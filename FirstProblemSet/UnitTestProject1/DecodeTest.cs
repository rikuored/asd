﻿using System;
using FirstPS;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstPSTest
{
	[TestClass]
	public class DecodeTest
	{
		[TestMethod]
		public void DecodeReturnRightArray()
		{
			var array = new int[] { 0, 1, 5, 0, 3, 2, 0, 66, 0, 0, 0 };
			CompareArrays(array, 11, 5);
		}
		[TestMethod]
		public void EmptyArray()
		{
			var array = new int[0];
			CompareArrays(array, 0, 0);
		}
		[TestMethod]
		public void ArrayContainsOnlyZeros()
		{
			var array = new int[] { 0, 0, 0, 0, 0, 0, };
			CompareArrays(array, 6, 0);
		}
		private void CompareArrays (int[] array, int expArrLength, int expLength)
		{
			var vector = new VectorCode(array);
			Assert.AreEqual(expArrLength, vector.OrigArrLength);
			Assert.AreEqual(expLength, vector.Length);
			Assert.IsTrue(Comparing.AreArraysSame(array, vector.Decode()));
		}

	}
}
