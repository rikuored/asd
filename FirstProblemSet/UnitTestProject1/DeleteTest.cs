﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstPS;

namespace FirstPSTest
{
	[TestClass]
	public class DeleteTest
	{
		[TestMethod]
		public void DeleteFromEmptyList()
		{
			var vector = new  VectorCode(new int[0]);
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => vector.Delete(0));
		}
		[TestMethod]
		public void DeleteElementWhichNotExcists()
		{
			var arr = new int[] { 1, 2, 3, 0, 4 };
			CompareArrays(arr, arr, 3);
		}
		[TestMethod]
		public void DeleteOutOfRange()
		{
			var arr = new int[] { 1, 2, 3, 0, 4 };
			var vector = new VectorCode(arr);
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => vector.Delete(-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => vector.Delete(5));
		}
		[TestMethod]
		public void DeleteInTheMiddle()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArray = new int[] { 1, 2, 0, 4 };
			CompareArrays(arr, expArray, 2);
		}
		[TestMethod]
		public void DeleteInTheEnd()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArray = new int[] { 1, 2, 3, 0 };
			CompareArrays(arr, expArray, 3);
		}
		[TestMethod]
		public void DeleteInTheStart()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArray = new int[] { 0, 2, 3, 4 };
			CompareArrays(arr, expArray, 0);
		}
		private void CompareArrays(int[] array, int[] expArray, int pos)
		{
			var vector = new VectorCode(array);
			vector.Delete(pos);
			Assert.IsTrue(Comparing.AreArraysSame(expArray, vector.Decode()));
		}

	}
}
