﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

namespace ASDissimoDoubleKill
{
	public class LinkedListMergeSort
	{
		public static int Counter { get; set; }
		public static  Stopwatch sw = new Stopwatch();
		public static void MergeSort<T>(ref MyLinkedList<T> headRef) where T : IComparable
		{
			Counter++;
			MyLinkedList<T> head = headRef;
			MyLinkedList<T> a = new MyLinkedList<T>();
			MyLinkedList<T> b = new MyLinkedList<T>();
			if ((head == null) || (head.Next == null))
			{
				return;
			}
			FrontBackSplit(head, ref a, ref b);
			if (a!=null&&a.Next!=null)MergeSort(ref a);
			if (b != null && b.Next != null) MergeSort(ref b);
			headRef = SortedMerge(a, b);
		}

		private static MyLinkedList<T> SortedMerge<T>(MyLinkedList<T> a, MyLinkedList<T> b) where T : IComparable
		{
			Counter++;
			MyLinkedList<T> result = null;
			if (a == null) return (b);
			else if (b == null) return (a);
			if (a.Item.CompareTo(b.Item) <= 0)
			{
				result = a;
				result.Next = SortedMerge(a.Next, b);
			}
			else
			{
				result = b;
				result.Next = SortedMerge(a, b.Next);
			}
			return result;
		}

		private static void FrontBackSplit<T>(MyLinkedList<T> source, ref MyLinkedList<T> frontRef, ref MyLinkedList<T> backRef) where T : IComparable
		{
			int len = source.Length;
			MyLinkedList<T> current = source;
			if (len < 2)
			{
				frontRef = source;
				frontRef.Length = len;
				backRef = null;
				backRef.Length = 0;
			}
			else
			{
				int hopCount = (len - 1) / 2;
				for (int i = 0; i < hopCount; i++)
				{
					current = current.Next;
					Counter++;
				}
				frontRef = source;
				frontRef.Length = hopCount + 1;
				backRef = current.Next;
				backRef.Length = len - hopCount - 1;
				current.Next = null;
			}

		}
	}
}
