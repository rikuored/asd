﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDissimoDoubleKill
{
	public class Data
	{
		public static void TakeData()
		{
			var hs = new string[10];
			var ls = new string[10];
			var ms = new string[15];
			for (int i = 10000; i <= 100000; i += 10000)
			{
				var arr = MakeArr(i);
				var arr2 = arr.ToArray();
				HeapSort.Sort(arr);
				var arr3 = LibrarySort.Sort(arr2);
				hs[i / 10000 - 1] = $"{i}, {HeapSort.swRes} ms, {HeapSort.Counter} itt";
				ls[i / 10000 - 1] = $"{i}, {LibrarySort.swRes} ms, {LibrarySort.Counter} itt";
			}

			for (int j = 1000; j <= 10000; j += 1000)
			{
				var linkedlist = new MyLinkedList<int>(MakeArr(j));
				linkedlist = linkedlist.Next;
				LinkedListMergeSort.Counter = 0;
				LinkedListMergeSort.sw.Restart();
				LinkedListMergeSort.MergeSort(ref linkedlist);
				LinkedListMergeSort.sw.Stop();
				ms[j / 1000 - 1] = $"{j}, {LinkedListMergeSort.sw.ElapsedMilliseconds} ms, {LinkedListMergeSort.Counter} itt";
			}

			File.Delete(@"Heapsort.txt");
			File.WriteAllLines(@"Heapsort.txt", hs);

			File.Delete(@"Librarysort.txt");
			File.WriteAllLines(@"Librarysort.txt", ls);

			File.Delete(@"Mergesort.txt");
			File.WriteAllLines(@"Mergesort.txt", ms);

		}
		private static int[] MakeArr(int count)
		{
			var arr = new int[count];
			for (int i = 0; i < count; i++)
			{
				var rnd = new Random();
				arr[i] = rnd.Next(413) + 1;
			}
			return arr;
		}
	}
}
