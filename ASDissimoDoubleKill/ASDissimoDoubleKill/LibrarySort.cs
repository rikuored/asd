﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDissimoDoubleKill
{
	public class LibrarySort
	{
		public static int index { get; private set; }
		public static long Counter { get; private set; }
		public static long swRes { get; private set; }
		public static T[] Sort<T>(T[] arr) where T : IComparable
		{
			Counter = 0;
			var sw = new Stopwatch();
			sw.Start();
			var n = arr.Length;
			var res = new T[n];
			index = n - 1;
			for (int i = 0; i < n; i++)
			{
				var t = BinarySearch(arr[i], res, (n - i - 1), n - 1);
				if (t < index) index = t;
				if (res[t] != null && res[t].CompareTo(default(T)) != 0)
				{
					for (int j = index; j <= t; j++)
					{
						res[j - 1] = res[j];
						Counter++;
					}
					index--;
				}
				res[t] = arr[i];
				Counter++;
			}
			sw.Stop();
			swRes = sw.ElapsedMilliseconds;
			return res;

		}

		private static int BinarySearch<T>(T item, T[] res, int left, int right) where T : IComparable
		{
			Counter++;
			if (right < left)
			{
				return right;
			}
			var mid = (left + right) / 2;
			if (item.CompareTo(res[mid]) > 0)
			{
				return BinarySearch(item, res, mid + 1, right);

			}
			else if (item.CompareTo(res[mid]) < 0)
			{
				return BinarySearch(item, res, left, mid - 1);
			}
			else
				return mid;
		}
	}
}
