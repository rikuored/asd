﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstPSTest
{
	public static class Comparing
	{
		public static bool AreArraysSame(int[] arr1, int[] arr2)
		{
			if (arr1.Length != arr2.Length) return false;
			for (int i = 0; i < arr1.Length; i++)
			{
				if (arr1[i] != arr2[i]) return false;
			}
			return true;
		}
	}
}
