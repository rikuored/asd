﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FirstPS
{
	class Program
	{
		static void Main(string[] args)
		{
			var arr = new int[] { 0, 5, 0, 3, 2, 0, 66, 0, 0, 0 };
			var vector = new VectorCode(arr);
			DemonstrateMethodWork("Original", vector);
			var arr2 = new int[] {0, 0, 3, 7, 1, 10, 11, 0, 0, 2 };
			var vector2 = new VectorCode(arr2);
			DemonstrateMethodWork("Second", vector2);
			vector.Insert(5,0);
			DemonstrateMethodWork("Insert element in", vector);
			vector.Delete(0);
			DemonstrateMethodWork("Delete element from", vector);
			vector.Mult(5, 2);
			DemonstrateMethodWork("Mult element on number in", vector);
			var scal = vector.ScalarProduct(vector2);
			Console.WriteLine("Scalar multiply of two vectors");
			Console.WriteLine(scal);
			var sumVector = vector.Sum(vector2);
			DemonstrateMethodWork("Sum original vector and second", sumVector);
			sumVector = sumVector.VectorSum();
			DemonstrateMethodWork("Strange sum of", sumVector);
		}
		public static void DemonstrateMethodWork(string method, VectorCode vector)
		{
			Console.WriteLine($"{method} vector");
			var arr = vector.Decode();
			foreach (var num in arr)
				Console.Write(num + " ");
			Console.WriteLine();
		}
	}
}
