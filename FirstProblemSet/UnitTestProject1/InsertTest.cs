﻿using System;
using FirstPS;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace FirstPSTest
{
	[TestClass]
	public class InsertTest
	{
		[TestMethod]
		public void InsertOutOfRange()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var vector = new VectorCode(arr);
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => vector.Insert(3,-1));
			Assert.ThrowsException<ArgumentOutOfRangeException>(() => vector.Insert(3,4));
		}
		[TestMethod]
		public void InsertInFreePosition()
		{
			var arr = new int[] { 1, 2, 0, 0, 3, 4 };
			var expArr = new int[] { 1, 2, 5, 0, 3, 4 };
			CompareArrays(arr, expArr, 5, 2);

		}
		[TestMethod]
		public void InsertInNotFreePosition()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArr = new int[] { 1, 2, 5, 4 };
			CompareArrays(arr, expArr, 5, 2);

		}

		[TestMethod]
		public void InsertInFirstPosition()
		{
			var arr = new int[] { 0, 0, 1, 2, 3, 4 };
			var expArr = new int[] { 3, 0, 1, 2, 3, 4 };
			CompareArrays(arr, expArr, 3, 0);
		}
		[TestMethod]
		public void InsertInLastPosition()
		{
			var arr = new int[] { 0, 1, 2, 3, 4, 0 };
			var expArr = new int[] { 0, 1, 2, 3, 4, 5 };
			CompareArrays(arr, expArr, 5, 5);
		}
		private void CompareArrays(int[] array, int[] expArray, int value, int pos)
		{
			var vector = new VectorCode(array);
			vector.Insert(value, pos);
			Assert.IsTrue(Comparing.AreArraysSame(expArray, vector.Decode()));
		}
	}
}
