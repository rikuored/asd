﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDissimoDoubleKill
{
	public class HeapSort
	#region

	{
		public static Stopwatch sw = new Stopwatch();
		public static long swRes { get; private set; }

		public static void Sort<T>(T[] arr) where T : IComparable<T>
		{
			sw.Restart();
			Counter = 0;
			BuildHeap<T>(arr);
			for (int i = HeapSize; i > 0; i--)
			{
				Swap(arr, HeapSize, 0);
				HeapSize--;
				HeapSwap(arr, 0);
				Counter++;
			}
			sw.Stop();
			swRes = sw.ElapsedMilliseconds;
		}

		public static int HeapSize { get; private set; }
		public static int Counter { get; private set; }
		private static void BuildHeap<T>(T[] arr) where T : IComparable<T>
		{
			Counter++;
			HeapSize = arr.Length - 1;
			for (int i = HeapSize / 2; i >= 0; i--)
				HeapSwap(arr, i);
		}

		private static void HeapSwap<T>(T[] arr, int i) where T : IComparable<T>
		{
			Counter++;
			int left = 2 * i + 1;
			int right = 2 * i + 2;
			int largest = i;

			if (left <= HeapSize && arr[left].CompareTo(arr[i]) > 0)
				largest = left;
			if (right <= HeapSize && arr[right].CompareTo(arr[largest]) > 0)
				largest = right;
			if (largest != i)
			{
				Swap(arr, i, largest);
				HeapSwap<T>(arr, largest);
			}

		}
		private static void Swap<T>(T[] arr, int i, int n)
		{
			T temp = arr[i];
			arr[i] = arr[n];
			arr[n] = temp;
		}

	}
	#endregion
}
	
