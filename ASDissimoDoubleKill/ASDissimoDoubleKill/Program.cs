﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASDissimoDoubleKill
{
	class Program
	{
		static void Main(string[] args)
		{
			var list = MakeArr(1000);
			var l = list.ToArray();
			var l2 = list.ToArray();
			HeapSort.Sort(list);
			Console.WriteLine("Heapsorted array:");
			foreach (var el in list)
				Console.Write($"{el} ");
			Console.WriteLine($"\nItterations: {HeapSort.Counter}\nTime:{HeapSort.swRes} ms");
			Console.Write($"Heapsorting of Harry Potter's books, please wait a couple of seconds...");
			HeapSort.Sort(MakeArr("harry.txt"));
			Console.WriteLine($"\rHeapsorting of Harry Potter's books time - {HeapSort.swRes} ms, itterations - {HeapSort.Counter}");


			var list2 = LibrarySort.Sort(l);
			Console.WriteLine("Librarysorted array:");
			foreach (var el in list2)
				Console.Write($"{el} ");
			Console.WriteLine($"\nItterations: {LibrarySort.Counter}\nTime:{LibrarySort.swRes} ms");

			var list3 = new MyLinkedList<int>(l);
			list3 = list3.Next;
			LinkedListMergeSort.sw.Restart();
			LinkedListMergeSort.MergeSort(ref list3);
			LinkedListMergeSort.sw.Stop();
			var arr = MyLinkedList<int>.Decode(list3);
			Console.WriteLine("Mergesorted array(linkedlist):");
			foreach (var e in arr)
				Console.Write($"{e} ");
			Console.WriteLine($"\nItterations: {LinkedListMergeSort.Counter}\nTime:{LinkedListMergeSort.sw.ElapsedMilliseconds} ms");

			Console.WriteLine($"Standart sort time: {StandartSortMeasure(l2)} ms");
		}
		private static int[] MakeArr(int count)
		{
			var list = new int[count];
			var rnd = new Random();
			Console.WriteLine("Unsorted array:");
			for (int i = 0; i < count; i++)
			{
				var pr = rnd.Next(413)+1;
				list[i] = pr;
				Console.Write(pr + " ");
			}
			Console.WriteLine();
			return list;
		}

		private static string[] MakeArr(string fileName)
		{
			return
				File.ReadLines(fileName)
				.SelectMany(str => str.Split(' ', ',', '"', '.', ':', '?', '!', '<', '>', '-').Where(sr => !string.IsNullOrEmpty(sr)))
				.ToArray();
		}
		public static long StandartSortMeasure<T>(T[] arr)

		{
			var sw = new Stopwatch();
			sw.Start();
			Array.Sort(arr);
			sw.Stop();
			return sw.ElapsedMilliseconds;
		}
	}
}
