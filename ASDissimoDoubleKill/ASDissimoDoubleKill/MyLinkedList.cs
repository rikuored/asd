﻿using System;

namespace ASDissimoDoubleKill
{
	public class MyLinkedList<T> where T : IComparable
	{
		public T Item { get; set; }
		public MyLinkedList<T> Next { get; set; }
		private  MyLinkedList<T> _tail;
		public int Length { get; set; }
		private static int TotalLength;
		public MyLinkedList(T[] arr)
		{
			for (int i = 0; i < arr.Length; i++)
			{
				MakeVectorChain(arr[i], arr.Length);
			}
			TotalLength = arr.Length;
		}
		public MyLinkedList()
		{
			Item = default(T);
			Next = null;
			Length = 0;
		}
		public static T[] Decode(MyLinkedList<T> list)
		{
			var arr = new T[TotalLength];
			var i = 0;
			var node = list;
			while (node != null)
			{
				arr[i] = node.Item;
				node = node.Next;
				i++;
			}
			return arr;
		}
		private void MakeVectorChain(T value, int index)
		{
			if (Next == null)
			{
				_tail = Next = new MyLinkedList<T>() { Item = value, Next = null, Length = index };
			}
			else
			{
				_tail = _tail.Next = new MyLinkedList<T> { Item = value, Next = null, Length = index };
			}
			Length++;
		}
	}

}
