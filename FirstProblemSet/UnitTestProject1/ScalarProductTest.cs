﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstPS;

namespace FirstPSTest
{
	[TestClass]
	public class ScalarProductTest
	{
		[TestMethod]
		public void MultiplyOfVectorsWithDifferentLength()
		{
			var vector1 = new VectorCode(new int[] { 1, 2, 3 } );
			var vector2 = new VectorCode(new int[] { 1, 2, 3, 4 });
			Assert.ThrowsException<ArgumentException>(() => vector1.ScalarProduct(vector2));
		}
		[TestMethod]
		public void UsualMultiply()
		{
			var arr1 = new int[] { 1, 2, 3, 4 };
			CompareScalar(arr1, arr1, 30);
		}
		[TestMethod]
		public void MulyiplyWithDifferentCoordinates()
		{
			var arr1 = new int[] { 0, 5, 7, 2, 0, 8 };
			var arr2 = new int[] { 3, 2, 0, 1, 0, 7 };
			CompareScalar(arr1, arr2, 68);
		}
		[TestMethod]
		public void MultiplyOfEmptyVectors()
		{
			var arr1 = new int[0];
			var arr2 = new int[0];
			CompareScalar(arr1, arr2, 0);
		}
		private void CompareScalar(int[] array, int[] array2, int expScal)
		{
			var vector = new VectorCode(array);
			var vector2 = new VectorCode(array2);
			Assert.AreEqual(expScal, vector.ScalarProduct(vector2));
		}
	}
}
