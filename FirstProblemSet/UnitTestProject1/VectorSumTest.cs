﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstPS;

namespace FirstPSTest
{
	[TestClass]
	public class VectorSumTest
	{
		[TestMethod]
		public void EmptyVectorSum()
		{
			var arr = new int[0];
			CompareSummedArrays(arr, arr);
		}
		[TestMethod]
		public void UsualVectorSum()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArr = new int[] { 4, 7, 9, 10 };
			CompareSummedArrays(arr, expArr);
		}
		[TestMethod]
		public void SumVectorWithZeros()
		{
			var arr = new int[] { 1, 0, 0, 4, 7, 9, 0, 1, 0 };
			var expArr = new int[] { 0, 1, 1, 10, 17, 21, 21, 21, 22};
			CompareSummedArrays(arr, expArr);
		}
		[TestMethod]
		public void ZeroVectorSum()
		{
			var arr = new int[] { 0, 0, 0, 0, 0 };
			var expArr = new int[] { 0, 0, 0, 0, 0 };
			CompareSummedArrays(arr, expArr);
		}
		private void CompareSummedArrays(int[] array, int[] expArr)
		{
			var vector = new VectorCode(array);
			Assert.IsTrue(Comparing.AreArraysSame(expArr, vector.VectorSum().Decode()));
		}
	}
}
