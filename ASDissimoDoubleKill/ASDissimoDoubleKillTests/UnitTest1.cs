﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using NUnit.Framework;
using ASDissimoDoubleKill;
using System.Diagnostics;

namespace ASDissimoDoubleKillTests
{
	[TestFixture]
	public class HeapSortTest
	{
		[Test]
		public void HeapSortNumbers()
		{
			var list = Help.MakeArr(100);
			HeapSort.Sort(list);
			Help.Compare(list);
		}
		[Test]
		public void HeapSortALotOfNumbers()
		{
			var list = Help.MakeArr(1000000);
			HeapSort.Sort(list);
			Help.Compare(list);
		}
		[Test]
		public void HeapSortALotWords()
		{
			var list = Help.MakeArr("harry.txt");
			HeapSort.Sort(list);
			Help.Compare(list);
		}
		[Test]
		public void HeapSortReversedNum()
		{
			var list = Help.MakeRevArr(1000000);
			HeapSort.Sort(list);
			Help.Compare(list);
		}
		[Test]
		public void HeapSortAlmostSortedNumbers()
		{
			var list = Help.MakeArr("almostsortednumbers.txt");
			HeapSort.Sort(list);
			Help.Compare(list);
		}
	}
	[TestFixture]
	public class LibrarySorted
	{
		[Test]
		public void LibrarySortedNumbers()
		{
			var list = LibrarySort.Sort(Help.MakeArr(100));
			Help.Compare(list);
		}
		[Test]
		public void LibrarySorttALotOfNumbers()
		{
			var list = LibrarySort.Sort(Help.MakeArr(100000));
			Help.Compare(list);
		}
		//[Test]
		//public void LibrarySortALotWords()
		//{
		//	var list = LibrarySort.Sort(Help.MakeArr("harry.txt"));
		//	Help.Compare(list);
		//}
		[Test]
		public void LibrarySortReversedNum()
		{
			var list = LibrarySort.Sort(Help.MakeRevArr(1000000));
			Help.Compare(list);
		}
		[Test]
		public void LibrarySortAlmostSortedNumbers()
		{
			var list = LibrarySort.Sort(Help.MakeArr("almostsortednumbers.txt"));
			Help.Compare(list);
		}
	}
	[TestFixture]
	public class LinkedListMergeSortTest
	{
		[Test]
		public void LinkedListMergeSortedNumbers()
		{

			var list = new MyLinkedList<int>(Help.MakeArr(100));
			list = list.Next;
			LinkedListMergeSort.MergeSort(ref list);
			Help.Compare(MyLinkedList<int>.Decode(list));
		}
		[Test]
		public void LinkedListMergeSortALotOfNumbers()
		{
			var list = new MyLinkedList<int>(Help.MakeArr(10000));
			list = list.Next;
			LinkedListMergeSort.MergeSort(ref list);
			Help.Compare(MyLinkedList<int>.Decode(list));
		}
		//[Test]
		//public void LinkedListMergeSortALotWords()
		//{
		//	var list = new MyLinkedList<string>(Help.MakeArr("harry.txt"));
		//	LinkedListMergeSort.MergeSort(ref list);
		//	Help.Compare(MyLinkedList<string>.Decode(list));
		//}
		[Test]
		public void LinkedListMergeSortReversedNum()
		{
			var list = new MyLinkedList<int>(Help.MakeRevArr(10000));
			list = list.Next;
			LinkedListMergeSort.MergeSort(ref list);
			Help.Compare(MyLinkedList<int>.Decode(list));
		}
		[Test]
		public void LinkedListMergeSorttAlmostSortedNumbers()
		{
			var list = new MyLinkedList<string>(Help.MakeArr("almostsortednumbers.txt"));
			list = list.Next;
			LinkedListMergeSort.MergeSort(ref list);
			Help.Compare(MyLinkedList<string>.Decode(list));
		}
	}

	class Help
	{
		public static int[] MakeRevArr(int count)
		{
			var list = new int[count];
			for (int i = count; i > 0; i--)
			{
				list[count - i] = i + 1;
			}
			Console.WriteLine();
			return list;
		}
		public static int[] MakeArr(int count)
		{
			var list = new int[count];
			var rnd = new Random();
			for (int i = 0; i < count; i++)
			{
				var pr = rnd.Next(15) + 1;
				list[i] = pr;
			}
			Console.WriteLine();
			return list;
		}
		public static string[] MakeArr(string fileName)
		{
			return
				File.ReadLines(fileName)
				.SelectMany(str => str.Split(' ', ',', '"', '.', ':', '?', '!', '<', '>', '-').Where(sr => !string.IsNullOrEmpty(sr)))
				.ToArray();
		}
		public static void Compare<T>(T[] list) where T: IComparable<T>
		{
			for (int i = 0; i < list.Length - 1; i++)
			{
				Assert.IsTrue(list[i].CompareTo(list[i + 1])<=0?true:false);
			}
		}
	}
}
