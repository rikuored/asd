﻿using System;

namespace FirstPS
{
	public class VectorCode
	{
		public int Length { get; private set; }
		public Element Head { get; private set; }
		private Element _tail;
		public int OrigArrLength { get; private set; }

		public VectorCode()
		{
			Length = 0;
			Head = null;
			_tail = null;
			OrigArrLength = 0;
		}
		public VectorCode(int[] arr)
		{
			for (int i = 0; i < arr.Length; i++)
			{
				if (arr[i] != 0)
				{
					MakeVectorChain(arr[i], i);
				}
			}
			OrigArrLength = arr.Length;
		}

		public int[] Decode()
		{
			var arr = new int[OrigArrLength];
			var node = Head;
			while (node != null)
			{
				arr[node.index] = node.Value;
				node = node.Next;
			}
			return arr;
		}
		public void Insert(int value, int pos)
		{
			Element current = Head;
			if (pos >= OrigArrLength || pos < 0) throw new ArgumentOutOfRangeException();
			if (current.index>=pos)
			{
				var item = new Element() { index = pos, Value = value, Next = Head };
				Head = item;
				Length++;
			}
			else
			{
				while (current != null)
				{
					if (current.index <= pos && current.Next != null && current.Next.index > pos)
					{
						var item = new Element() { index = pos, Value = value, Next = current.Next };
						current.Next = item;
						if (current.index != pos)
							Length++;
						break;
					}
					else if (current.Next == null)
					{
						var item = new Element() { index = pos, Value = value, Next = null };
						current.Next = item;
						_tail = item;
						Length++;
						break;
					}
					current = current.Next;
				}
			}
		}
		public void Delete(int pos)
		{
			Element previous = null;
			Element current = Head;
			if (pos >= OrigArrLength || pos < 0) throw new ArgumentOutOfRangeException();
			if (current.index <= pos)
			{
				while (current != null)
				{
					if (current.index == pos)
					{
						if (previous != null)
						{
							previous.Next = current.Next;
							if (current.Next == null)
							{
								_tail = previous;
							}
						}
						else
						{
							Head = Head.Next;
							if (Head == null)
							{
								_tail = null;
							}
						}
						Length--;
						break;
					}
					previous = current;
					current = current.Next;
				}
			}
		}
		public int ScalarProduct(VectorCode vector)
		{
			if (OrigArrLength != vector.OrigArrLength) throw new ArgumentException
															("You can't multiply vectors with diferent dimensions!");
			Element node1 = Head;
			Element node2 = vector.Head;
			int scalar=0;
			while (node1!=null&&node2!=null)
			{
				if (node1.index == node2.index)
				{
					scalar += node1.Value * node2.Value;
					node1 = node1.Next;
					node2 = node2.Next;
				}
				else if (node1.index < node2.index)
					node1 = node1.Next;
				else
					node2 = node2.Next;
			}
			return scalar;
		}
		public VectorCode Sum (VectorCode vector)
		{
			if (OrigArrLength!= vector.OrigArrLength) throw new ArgumentException
															("You can't summarize vectors with diferent dimensions!");
			var node1 = Head;
			var node2 = vector.Head;
			VectorCode sumVec = new VectorCode();
			while (node1!=null&&node2!=null)
			{
				if (node1.index == node2.index)
				{
					sumVec.MakeVectorChain(node1.Value + node2.Value, node1.index);
					node1 = node1.Next;
					node2 = node2.Next;
					
				}
				else if (node1.index < node2.index)
				{
					sumVec.MakeVectorChain(node1.Value, node1.index);
					node1 = node1.Next;
				}
				else
				{
					sumVec.MakeVectorChain(node2.Value, node2.index);
					node2 = node2.Next;
				}
			}
			if (node1 != null)
				node1 = MakeChainFromVectorCoordinate(node1, sumVec);
			if (node2 != null)
				node2 = MakeChainFromVectorCoordinate(node2, sumVec);
			sumVec.OrigArrLength = OrigArrLength;
			return sumVec;
		}

		public VectorCode VectorSum()
		{
			var reversedVector = Reverse(Head);
			var node = reversedVector.Head;
			VectorCode sumVec = new VectorCode();
			int sum = 0;
			int index = 0;
			while (node != null)
			{
				if (node.index == index)
				{
						sum += node.Value;
						sumVec.MakeVectorChain(sum, index);
						node = node.Next;
				}
				else
					sumVec.MakeVectorChain(sum, index);
				index++;
			}
			while (index < OrigArrLength)
			{
				sumVec.MakeVectorChain(sum, index);
				index++;
			}
			sumVec.OrigArrLength = OrigArrLength;
			return sumVec;
		}
		public void Mult(int value, int num)
		{
			var current = Head;
			Element previous = null;
			while (current!=null)
			{
				if (current.Value == value)
				{
					if (num != 0)
					{
						if (previous != null)
						{
							current.Value = current.Value*num;
							previous.Next = current;
						}
						else
							Head.Value = Head.Value*num;
					}
					else
					{
						if (previous != null)
						{
							previous.Next = current.Next;
							if (current.Next == null)
							{
								_tail = previous;
							}
						}
						else
						{
							Head = Head.Next;
							if (Head == null)
							{
								_tail = null;
							}
						}
						Length--;
					}
				}
				previous = current;
				current = current.Next;
			}
		}

		private Element MakeChainFromVectorCoordinate(Element node, VectorCode sumVec)
		{
			sumVec.MakeVectorChain(node.Value, node.index);
			node = node.Next;
			return node;
		}

		private void SumOneVector(Element node, VectorCode sumVec)
		{
			while (node != null)
				node = MakeChainFromVectorCoordinate(node, sumVec);
		}
		private void MakeVectorChain(int value, int index)
		{
			if (Head == null)
			{
				_tail = Head = new Element() { Value = value, Next = null, index = index };
			}
			else
			{
				_tail = _tail.Next = new Element { Value = value, Next = null, index = index };
			}
			Length++;
		}
		private void AddFirst(Element element, int length)
		{
			Element item;
			if (Head!=null)
				item = new Element() { index = length - element.index-1,
										Value = element.Value, Next = Head };
			else
				item = new Element() { index = length - element.index-1,
										Value = element.Value, Next = null };
			Head = item;
		}
		 private VectorCode Reverse(Element element)
		{
			var node = element;
			var revList = new VectorCode();
			while (node!=null)
			{
				revList.AddFirst(node, OrigArrLength);
				node = node.Next;
			}
			return revList;
		}
	}
}
