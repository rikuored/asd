﻿namespace FirstPS
{
	public class Element
	{
		public Element Next { get; set; }
		public int index { get; set; }
		public int Value { get; set; }
	}
}
