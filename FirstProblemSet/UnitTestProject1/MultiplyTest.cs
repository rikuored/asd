﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FirstPS;

namespace FirstPSTest
{
	[TestClass]
	public class MultiplyTest
	{
		[TestMethod]
		public void MultiplyOneElement()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			var expArr = new int[] { 1, 10, 3, 4 };
			CompareArrays(arr, expArr, 2, 5, 4);
		}
		[TestMethod]
		public void MultiplyFewElements()
		{
			var arr = new int[] { 1, 2, 2, 2, 3, 7, 22, 12, 8, 2 };
			var expArr = new int[] { 1, 10, 10, 10, 3, 7, 22, 12, 8, 10 };
			CompareArrays(arr, expArr, 2, 5, 10);
		}
		[TestMethod]
		public void MultiplyNonexistentElement()
		{
			var arr = new int[] { 1, 2, 3, 4 };
			CompareArrays(arr, arr, 5, 2, 4);
		}
		public void MultiplyEmptyVector()
		{
			var arr = new int[0];
			CompareArrays(arr, arr, 2, 2, 0);
		}
		[TestMethod]
		public void MultiplyOnZero()
		{
			var arr = new int[] { 1, 2, 0, 0, 3, 2, 5, 2, 4, 0, 2, 0 };
			var expArr = new int[] { 1, 0, 0, 0, 3, 0, 5, 0, 4, 0, 0, 0 };
			CompareArrays(arr, expArr, 2, 0, 4);
		}
		private void CompareArrays(int[] array, int[] expArray, int value, int num, int expLength)
		{
			var vector = new VectorCode(array);
			vector.Mult(value, num);
			Assert.IsTrue(Comparing.AreArraysSame(expArray, vector.Decode()));
			Assert.AreEqual(expLength, vector.Length);
		}
	}
}
